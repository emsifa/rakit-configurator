<?php

use Rakit\Configurator\Configurator;

class ConfiguratorTest extends PHPUnit_Framework_TestCase {

    protected $config;

    public function setUp() {
        $this->config = new Configurator();
    }

    public function tearDown() {
        $this->config = null;
    }

    public function testSet()
    {   
        $this->config->set("foo.bar", "foobar");
        
        $configs = $this->config->all();

        $this->assertEquals($configs['foo']['bar'], "foobar");
    }

    public function testSetDotNotation()
    {
        $this->config["foo.bar"] = "foobar";

        $configs = $this->config->all();

        $this->assertEquals($configs['foo']['bar'], "foobar");
    }

    public function testGet()
    {   
        $this->config->set("foo.bar", "foobar");

        $value = $this->config->get("foo.bar");
        $undefined = $this->config->get("undefined.key");
        $default_value = $this->config->get("undefined.key", "default value");

        $this->assertEquals($value, "foobar");
        $this->assertEquals(NULL, $undefined);
        $this->assertEquals($default_value, "default value");
    }

    public function testGetDotNotation()
    {
        $this->config->set("foo.bar", "foobar");

        $value = $this->config["foo.bar"];
        $undefined = $this->config["undefined.key"];

        $this->assertEquals($value, "foobar");
    }

    public function testHas()
    {
        $this->config->set("foo.bar", "foobar");        

        $this->assertTrue($this->config->has("foo.bar"));
        $this->assertFalse($this->config->has("undefined.key"));
    }

    public function testNamespace()
    {
        // add namespace store
        $this->config->store = array(
            'store_name' => 'my store',
            'database' => array(
                'host' => 'localhost',
                'username' => 'db_user',
                'password' => 'db_password',
                'dbname' => 'db_store'
            )
        );

        // get simple value from namespace
        $store_name = $this->config->store->get("store_name");
        $this->assertEquals($store_name, "my store");

        // get value using dot notation
        $db_host = $this->config->store["database.host"];
        $this->assertEquals($db_host, "localhost");

        // set value using set
        $this->config->store->set("database.host", "new_db_host");
        // set value using dot notation
        $this->config->store->set("database.dbname", "new_store_db");

        // get all changed configs store
        $configs = $this->config->store->all();

        $this->assertEquals($configs["database"]["host"], "new_db_host");
        $this->assertEquals($configs["database"]["dbname"], "new_store_db");
    }

}