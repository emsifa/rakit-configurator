<?php

namespace Rakit\Configurator;

use ArrayAccess;
use Rakit\Util\Arr;

class Configurator implements ArrayAccess {

    protected $configs = array();
    protected $namespaces = array();
     
    public function __construct(array $initial_values = array()) {
        $this->configs = $initial_values;
    }
     
    public function has($key) {
        return Arr::has($this->configs, $key);
    }

    public function set($key, $value) {
        if(is_array($key)) {
            $this->configs = $key;
            return;
        }
        
        Arr::set($this->configs, $key, $value);
    }

    public function get($key, $default = null) {
        return Arr::get($this->configs, $key, $default);
    }
     
    public function remove($key) {
        Arr::remove($this->configs, $key);
    }

    public function all() {
        return $this->configs;
    }

    public function offsetSet($key, $value) {
        $this->set($key, $value);
    }

    public function offsetExists($key) {
        return $this->has($key);
    }

    public function offsetUnset($key) {
        $this->remove($key);
    }

    public function offsetGet($key) {
        return $this->get($key, null);
    }
     
    public function __set($namespace, $value) {
        $configurator = new Configurator();
        $this->namespaces[$namespace] = $configurator;
       
        if(is_array($value)) {
            $configurator->set($value, null);
        }
    }

    public function __get($namespace) {
        if(isset($this->namespaces[$namespace])) {
            return $this->namespaces[$namespace];
        } else {
            throw new \Exception("Undefined config namespace {$namespace}");
        }
    }

}
